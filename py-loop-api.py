#/usr/bin/python
# -*- coding: utf-8 -*-


import socket
import json
import sys
from pymongo import MongoClient
# from interruptingcow import timeout


client = MongoClient("mongodb://127.0.0.1:27017")
db = client['mining']

 

def linesplit(socket):
	buffer = socket.recv(4096)
	done = False
	while not done:
		more = socket.recv(4096)
		if not more:
			done = True
		else:
			buffer = buffer+more
	if buffer:
		return buffer


if __name__ == "__main__":

	# Команда у API (пр. pools, summary...)
	api_command = sys.argv[1].split('|')

	# Если указан IP PORT
	if len(sys.argv) < 3:
		api_ip = '192.168.66.1'
		api_port = 4028
	else:
		api_ip = sys.argv[2]
		print ("api_ip:", api_ip)
		api_port = sys.argv[3]
		print ("ip_port:,", api_port)


	s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.settimeout(0.5)
	s.connect((api_ip,int(api_port)))
	s.settimeout(None)


	if len(api_command) == 2:
		s.send(json.dumps({"command":api_command[0],"parameter":api_command[1]}))
	else:
		s.send(json.dumps({"command":api_command[0]}))

	response = linesplit(s)
	response = response.replace('\x00','')
	response = json.loads(response)

	# Добавить к записи IP Майнера, с которого получена инфа
	response["api_port"] = api_port
	response["api_ip"] = api_ip

	# Добавить в базу данных
	db[str(api_command[0])].insert(response)

	s.close()

